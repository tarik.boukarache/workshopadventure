# Krew

[Krew](https://github.com/kubernetes-sigs/krew) est un outil qui va vous permettre d'améliorer votre arme `kubectl` afin de pouvoir satisfaire vos envies de grandeur. Il a été préinstallé dans votre livre de sorts et quelques améliorations ont déjà été mises en place afin que votre voyage se passe de la meilleure façon possible. Vous pouvez dès à présent regarder les améliorations fournies via `kubectl krew list`.

## Installer krew

https://krew.sigs.k8s.io/docs/user-guide/setup/install/

## Installer un plugin

```sh
kubectl krew install <nom du plugin>
# ex: kubectl krew install ctx
```

## Mettre à jour ses plugins

```sh
kubectl krew upgrade
```

## Supprimer un plugin

```sh
kubectl krew uninstall <nom du plugin>
# ex: kubectl krew uninstall ctx
```

## Rechercher un plugin

```sh
$ kubectl krew search secret
NAME            DESCRIPTION                                        INSTALLED
modify-secret   modify secret with implicit base64 translations    no
secretdata      Viewing decoded Secret data with search flags      no
ssm-secret      Import/export secrets from/to AWS SSM param store  no
view-secret     Decode Kubernetes secrets                          no
whisper-secret  Create secrets with improved privacy               no
```

## Avoir des informations sur un plugin

```sh
$ kubectl krew info ctx
NAME: ctx
INDEX: default
URI: https://github.com/ahmetb/kubectx/archive/v0.9.4.tar.gz
SHA256: 91e6b2e0501bc581f006322d621adad928ea3bd3d8df6612334804b93efd258c
VERSION: v0.9.4
HOMEPAGE: https://github.com/ahmetb/kubectx
DESCRIPTION:
Also known as "kubectx", a utility to switch between context entries in
your kubeconfig file efficiently.
```
